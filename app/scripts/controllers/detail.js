'use strict';

/**
 * @ngdoc function
 * @name angularLearningApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularLearningApp
 */
angular.module('angularLearningApp')
    .controller('DetailCtrl', function ($scope) {

        $scope.myName = 'Jin Li';

    });
