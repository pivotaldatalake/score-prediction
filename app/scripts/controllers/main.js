'use strict';

/**
 * @ngdoc function
 * @name angularLearningApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularLearningApp
 */
angular.module('angularLearningApp')

    .controller('MainCtrl', function ($scope,$http) {

        //global variable
        $scope.afterFilterDirectorList = [];//director list after filter. It is shown in input-box.
        $scope.allDirectorList = [];//all director list. Just to assist
        $scope.initialPage = 1;//if it is the first time to load page
        $scope.selectedDirector = 0;//whether selected an director
        $scope.directorScoreDict = {};//data to display. format: {'director':'Reed Guo', 'averageScore':5}









        // get data from google spreadsheets
        $scope.getData = function(){
            $scope.directorCastAllInfoDict = {};//format: {'Zhang Yimou':{'movie1':6,'movie2':8}}
            $http({
                method: 'GET',
                url: 'http://spreadsheets.google.com/feeds/list/1-HD_aiErHkt4LGk6Mj4r_Tul30c0YUIlNGmc2W4LHIc/od6/public/values?alt=json'
            })
            .then(function(response) {
                var allValues = response.data.feed.entry;
                for (var i = 0; i < allValues.length; i++) {
                    var rows = allValues[i];

                    var directorValue = rows['gsx$director']['$t'];
                    var movieNameValue = rows['gsx$moviename']['$t'];
                    var scoreValue = rows['gsx$score']['$t'];
                    if(null == $scope.directorCastAllInfoDict[directorValue]){
                        var tempDict = {};
                        tempDict[movieNameValue] = scoreValue;
                        $scope.directorCastAllInfoDict[directorValue] = tempDict;
                    }else{
                        var existedInfoDict = $scope.directorCastAllInfoDict[directorValue];
                        existedInfoDict[movieNameValue] = scoreValue;
                        $scope.directorCastAllInfoDict[directorValue] = existedInfoDict;
                    }
                }
                $scope.getDirectorList();

            });
        }

        //get director list
        $scope.getDirectorList = function(){
            var tempDirectorDict = {};
            for(var eachDirector in $scope.directorCastAllInfoDict){
                if (null == tempDirectorDict[eachDirector]){
                    $scope.allDirectorList.push(eachDirector);
                }
                tempDirectorDict[eachDirector] = 1;
            }
            $scope.initialPage = 0;
        }

        //search director based on typed director-name
        $scope.updateDirector = function(typed){
            if(typed!=""){
                console.log('Typed is ' + typed);
                var lowerTyped = typed.toLowerCase();
                var newDirectorList = [];
                for(var i=0;i<$scope.allDirectorList.length;i++){
                    if ($scope.allDirectorList[i].toLowerCase().indexOf(lowerTyped)>=0){
                        newDirectorList.push($scope.allDirectorList[i]);
                    }
                }
                $scope.afterFilterDirectorList = newDirectorList;
            }else{
                $scope.afterFilterDirectorList = [];
            }

        }

        //select a specific director
        $scope.selectDirector = function(sugession){
            console.log('sugession is ' + sugession);
            //get average score
            $scope.getAverageScore(sugession);
            $scope.selectedDirector = 1;
        }


        //get average score
        $scope.getAverageScore = function(director){
            var movieCastScoreDict = $scope.directorCastAllInfoDict[director];

            var movieCount = 0;
            var sumScore = 0;
            for(var eachMovie in movieCastScoreDict){
                movieCount ++;
                sumScore += parseFloat(movieCastScoreDict[eachMovie]);
            }
            var averageScore = sumScore/movieCount;
            $scope.directorScoreDict['director'] = director;
            $scope.directorScoreDict['averageScore'] = averageScore.toFixed(2);
        }

        //initialize
        $scope.getData();



    });
