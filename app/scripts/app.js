'use strict';

/**
 * @ngdoc overview
 * @name angularLearningApp
 * @description
 * # angularLearningApp
 *
 * Main module of the application.
 */
angular
  .module('angularLearningApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.sortable',
    'autocomplete'

  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/detail', {
        templateUrl: 'views/detail.html',
        controller: 'DetailCtrl'
      })
      .otherwise({
        redirectTo: '/main'
      });
  });
